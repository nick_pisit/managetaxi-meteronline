package projectgrails


class SettingComposer extends zk.grails.Composer {

    def afterCompose = { window ->

        def owner = Owner.findById(session.user)
	        $('#username').setText(owner.Username)
	        $('#password').setText(owner.Password)
	        $('#email').setText(owner.Email)
	        $('#phone').setText(owner.Phone)
	        $('#address').setText(owner.AddressOwner)
	        $('#stockname').setText(owner.StockName)

        def setting = Setting.findById(session.user)
	        $('#rate').setValue(setting.RateService)
	        $('#panality').setValue(setting.Penalty) 
	        $('#deposit').setValue(setting.Deposit)
	        $('#amouttotal').setValue(setting.AmoutTaxi)

        $('#btnUpdate1').on('click',{
        	def owner2 = Owner.findById(session.user)
	        owner2.Username = $('#username').text()
	        owner2.Password = $('#password').text()
	        owner2.Email = $('#email').text()
	        owner2.Phone = $('#phone').text()
	        owner2.AddressOwner = $('#address').text()
	        owner2.StockName = $('#stockname').text()
	        owner2.save()
	        alert("Update Success!!!")
	        })

        $('#btnUpdate2').on('click',{
	        setting.RateService = $('#rate').getValue()
	        setting.Penalty = $('#panality').getValue()
	        setting.Deposit = $('#deposit').getValue()
	        setting.AmoutTaxi = $('#amouttotal').getValue()
	        setting.save()
	        alert("Update Success!!!")
        	})
    }
}
