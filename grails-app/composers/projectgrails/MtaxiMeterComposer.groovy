package projectgrails


class MtaxiMeterComposer extends zk.grails.Composer {

    def afterCompose = { window ->
    	if((session.user).equals(null)){
    		$('#bordermain').setVisible(false)
        	redirect(uri : "/Login.zul")
        }
    	
        
                else{

                	def user = Owner.findById(session.user)
                	$('#nameowner').setValue(user.StockName)
                }
        if((session.controlpage).equals("pt") || (session.controlpage).equals("et")){
            $('#chp').setSrc("/proEditTaxi.zul")
        }
            
        if((session.controlpage).equals("member")){
            $('#chp').setSrc("/profileMember.zul")
        }
        if((session.controlpage).equals("pdf")){
            $('#chp').setSrc("/showpdf.zul")
        }

        if((session.controlpage).equals("map")){
            $('#chp').setSrc("/gmap.zul")
        }
        $('#logout').on('click',{
        	session.user = null
            session.controlpage = null
        	redirect(uri : "/Login.zul")
        	})
        $('#setting').on('click',{
            $('#chp').setSrc("/setting.zul")
            session.controlpage = null
            })

        $('#listtaxi').on('click',{
            $('#chp').setSrc("/listTaxi.zul")
            session.controlpage = null
            })
        $('#home').on('click',{
            session.controlpage = null
            $('#chp').setSrc("/listTaxi.zul")
            })
        $('#members').on('click',{
            session.controlpage = null
            $('#chp')setSrc("/listMembers.zul")
            })

         
        
    }
        
        }
    

