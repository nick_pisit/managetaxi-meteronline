package projectgrails
import org.zkoss.io.*
import org.zkoss.util.media.Media
import org.zkoss.zhtml.Fileupload
import org.zkoss.zk.ui.event.EventListener
import org.zkoss.zk.ui.event.UploadEvent
import org.zkoss.image.Image
import org.zkoss.image.AImage

class ListTaxiComposer extends zk.grails.Composer {
    int count = 0
    int cT = 0
    String StrStatus = ""
    String strNocarSearch = ""
    Media media
    Image img
    def afterCompose = { window ->
        $('#addform').setVisible(false)
        $('#btnAddcar').setVisible(false)
        $('#hideformaddcar').setVisible(false)
        $('#colorTaxi').setSelectedIndex(0)
        UpdateTaxi()
        
    	

        $('#addtaxi').on('click',{
        	$('#addform').setVisible(true)
        	$('#btnAddcar').setVisible(true)
        	$('#hideformaddcar').setVisible(true)
        	$('#addtaxi').setVisible(false)
        	})
        $('#hideformaddcar').on('click',{
        	$('#addform').setVisible(false)
	    	$('#btnAddcar').setVisible(false)
	    	$('#hideformaddcar').setVisible(false)
	    	$('#addtaxi').setVisible(true)
	    	$('#hideformaddcar').setVisible(false)
        	})
        $('#btnAddcar').on('click',{
            AddTaxi() 
                 
            })

        $('#upload').on('upload'){ ev->
            media = ev.getMedia()
            img = (Image)media;
            $('#imgs').setContent(img)
        }
        $('#c1').on('check',{
            $('#tablelisttaxi > rows > row').detach()
            count = 0
            returntoday()
            })
        $('#c2').on('check',{
            $('#tablelisttaxi > rows > row').detach()
            count = 0
            easy()
            })
        $('#c3').on('check',{
            $('#tablelisttaxi > rows > row').detach()
            count = 0
            busy()
            })
        $('#c4').on('check',{
            $('#tablelisttaxi > rows > row').detach()
            count = 0
            UpdateTaxi()
            })
        $('#btnsearchcar').on('click',{
            strNocarSearch = $('#searchcar').text()
            $('#tablelisttaxi > rows > row').detach()
            count = 0
            SearchNoCar()
            })
    }
    def AddTaxi(){
            $('#tablelisttaxi > rows > row').detach()
            count = 0
            String strnoCartxt = $('#noCartxt').text()
            String strtxtYeeho = $('#txtYeeho').text()
            String strcolorTaxi = String.valueOf($('#colorTaxi').getSelectedItem().getLabel())
            int status = 1
            if(strnoCartxt.equals("") || strtxtYeeho.equals("")){
                alert("ควรจะใส่ทะเบียน และยี่ห้อรถ")
            }
            else{
            def taxidetails = new ListDetailsTaxi()
            taxidetails.Registration = strnoCartxt
            taxidetails.Brand = strtxtYeeho
            taxidetails.Color = strcolorTaxi
            taxidetails.Status = status
            taxidetails.imageTaxi = $('#imgs')[0].getContent().getByteData()
            def taxistock = TaxiStock.findById(session.user)
            taxidetails.taxistock = taxistock

            //////////////image save////////////////
            //taxidetails.imageTaxi = ($('#imgs').getContent().getByteData()) 
            taxidetails.save()

            $('#noCartxt').setText("")
            $('#txtYeeho').setText("")
            $('#colorTaxi').setSelectedIndex(0)
            UpdateTaxi()
            
            
        } 
    }
    def UpdateTaxi(){

        for(ListDetailsTaxi findall : ListDetailsTaxi.findAll()){
            String strCompare = "projectgrails.TaxiStock : " + session.user
            String strTaxistock = String.valueOf(findall.taxistock)
            if(strCompare.equals(strTaxistock)){
                if(findall.Status == 1){
                    StrStatus = "ว่าง"
                }
                else if(findall.Status == 2){
                    StrStatus = "ไม่ว่าง"
                }
                else if(findall.Status == 3){
                    StrStatus = "คืนรถวันนี้"
                }
                count = count + 1
                cT = cT + 1
                $('#tablelisttaxi > rows').append{
                    row(style:"font-size:13px;color:black;background:#B0C4DE;"){
                        label(value:"${count}",style:"font-size:13px;color:black")
                        label(value:"ทะเบียน " + findall.Registration + "   ยี่ห้อ " + findall.Brand + "   สี" + findall.Color,
                        style:"font-size:13px;color:black")
                        button(id:"profile" + "${cT}" + "s" + findall.id,orient:"vertical",style:"font-size:13px;",label:"Profile",class:"btn btn-success")
                        button(id:"delete" + "${cT}" + "s" + findall.id,orient:"vertical",style:"font-size:13px;",label:"Remove",class:"btn btn-danger")
                        button(id:"edit" + "${cT}" + "s" + findall.id,orient:"vertical",style:"font-size:13px;",label:"Edit",class:"btn btn-warning")
                        label(value:"${StrStatus}",style:"font-size:13px;color:Green")
                    }

                }

                $('#tablelisttaxi > rows > row > button[label="Profile"]').on('click',{
                    String tranPro = String.valueOf(it.target.id)
                    String SubtranPro = tranPro.substring(tranPro.lastIndexOf("s")+1)
                    session.profileT = SubtranPro
                    session.controlpage = "pt"
                    redirect(uri: "/mtaxi-meter.zul")
                    })
                $('#tablelisttaxi > rows > row > button[label="Remove"]').on('click',{
                    String tranRev = String.valueOf(it.target.id)
                    String SubtranRev = tranRev.substring(tranRev.lastIndexOf("s")+1)
                    def RemoveTaxi = ListDetailsTaxi.findById(SubtranRev)
                    RemoveTaxi.delete()
                    redirect(uri: "/mtaxi-meter.zul")
                    })
                $('#tablelisttaxi > rows > row > button[label="Edit"]').on('click',{
                    String tranEd = String.valueOf(it.target.id)
                    String SubtranEd = tranEd.substring(tranEd.lastIndexOf("s")+1)
                    session.profileT = SubtranEd
                    session.controlpage = "et"
                    redirect(uri: "/mtaxi-meter.zul")
                    })
            }
            

        }


    }

 
    def returntoday(){
        for(ListDetailsTaxi findall : ListDetailsTaxi.findAll()){
            String strCompare = "projectgrails.TaxiStock : " + session.user
            String strTaxistock = String.valueOf(findall.taxistock)
            if(strCompare.equals(strTaxistock)){
                if(findall.Status == 3){
                    StrStatus = "คืนรถวันนี้"
                
                count = count + 1
                cT = cT + 1
                $('#tablelisttaxi > rows').append{
                    row(style:"font-size:13px;color:black;background:#B0C4DE;"){
                        label(value:"${count}",style:"font-size:13px;color:black")
                        label(value:"ทะเบียน " + findall.Registration + "   ยี่ห้อ " + findall.Brand + "   สี" + findall.Color,
                        style:"font-size:13px;color:black")
                        button(id:"profile" + "${cT}" + "s" + findall.id,orient:"vertical",style:"font-size:13px;",label:"Profile",class:"btn btn-success")
                        button(id:"delete" + "${cT}" + "s" + findall.id,orient:"vertical",style:"font-size:13px;",label:"Remove",class:"btn btn-danger")
                        button(id:"edit" + "${cT}" + "s" + findall.id,orient:"vertical",style:"font-size:13px;",label:"Edit",class:"btn btn-warning")
                        label(value:"${StrStatus}",style:"font-size:13px;color:Green")
                    }

                }

                $('#tablelisttaxi > rows > row > button[label="Profile"]').on('click',{
                    String tranPro = String.valueOf(it.target.id)
                    String SubtranPro = tranPro.substring(tranPro.lastIndexOf("s")+1)
                    session.profileT = SubtranPro
                    session.controlpage = "pt"
                    redirect(uri: "/mtaxi-meter.zul")
                    })
                $('#tablelisttaxi > rows > row > button[label="Remove"]').on('click',{
                    String tranRev = String.valueOf(it.target.id)
                    String SubtranRev = tranRev.substring(tranRev.lastIndexOf("s")+1)
                    def RemoveTaxi = ListDetailsTaxi.findById(SubtranRev)
                    RemoveTaxi.delete()
                    redirect(uri: "/mtaxi-meter.zul")
                    })
                $('#tablelisttaxi > rows > row > button[label="Edit"]').on('click',{
                    String tranEd = String.valueOf(it.target.id)
                    String SubtranEd = tranEd.substring(tranEd.lastIndexOf("s")+1)
                    session.profileT = SubtranEd
                    session.controlpage = "et"
                    redirect(uri: "/mtaxi-meter.zul")
                    })
            }

            }
            

        }
    }

    def easy(){
        for(ListDetailsTaxi findall : ListDetailsTaxi.findAll()){
            String strCompare = "projectgrails.TaxiStock : " + session.user
            String strTaxistock = String.valueOf(findall.taxistock)
            if(strCompare.equals(strTaxistock)){
                if(findall.Status == 1){
                    StrStatus = "ว่าง"
                
                count = count + 1
                cT = cT + 1
                $('#tablelisttaxi > rows').append{
                    row(style:"font-size:13px;color:black;background:#B0C4DE;"){
                        label(value:"${count}",style:"font-size:13px;color:black")
                        label(value:"ทะเบียน " + findall.Registration + "   ยี่ห้อ " + findall.Brand + "   สี" + findall.Color,
                        style:"font-size:13px;color:black")
                        button(id:"profile" + "${cT}" + "s" + findall.id,orient:"vertical",style:"font-size:13px;",label:"Profile",class:"btn btn-success")
                        button(id:"delete" + "${cT}" + "s" + findall.id,orient:"vertical",style:"font-size:13px;",label:"Remove",class:"btn btn-danger")
                        button(id:"edit" + "${cT}" + "s" + findall.id,orient:"vertical",style:"font-size:13px;",label:"Edit",class:"btn btn-warning")
                        label(value:"${StrStatus}",style:"font-size:13px;color:Green")
                    }

                }

                $('#tablelisttaxi > rows > row > button[label="Profile"]').on('click',{
                    String tranPro = String.valueOf(it.target.id)
                    String SubtranPro = tranPro.substring(tranPro.lastIndexOf("s")+1)
                    session.profileT = SubtranPro
                    session.controlpage = "pt"
                    redirect(uri: "/mtaxi-meter.zul")
                    })
                $('#tablelisttaxi > rows > row > button[label="Remove"]').on('click',{
                    String tranRev = String.valueOf(it.target.id)
                    String SubtranRev = tranRev.substring(tranRev.lastIndexOf("s")+1)
                    def RemoveTaxi = ListDetailsTaxi.findById(SubtranRev)
                    RemoveTaxi.delete()
                    redirect(uri: "/mtaxi-meter.zul")
                    })
                $('#tablelisttaxi > rows > row > button[label="Edit"]').on('click',{
                    String tranEd = String.valueOf(it.target.id)
                    String SubtranEd = tranEd.substring(tranEd.lastIndexOf("s")+1)
                    session.profileT = SubtranEd
                    session.controlpage = "et"
                    redirect(uri: "/mtaxi-meter.zul")
                    })
            }
            }
            

        }
    }

def busy(){
        for(ListDetailsTaxi findall : ListDetailsTaxi.findAll()){
            String strCompare = "projectgrails.TaxiStock : " + session.user
            String strTaxistock = String.valueOf(findall.taxistock)
            if(strCompare.equals(strTaxistock)){
                if(findall.Status == 2){
                    StrStatus = "ไม่ว่าง"
                
                count = count + 1
                cT = cT + 1
                $('#tablelisttaxi > rows').append{
                    row(style:"font-size:13px;color:black;background:#B0C4DE;"){
                        label(value:"${count}",style:"font-size:13px;color:black")
                        label(value:"ทะเบียน " + findall.Registration + "   ยี่ห้อ " + findall.Brand + "   สี" + findall.Color,
                        style:"font-size:13px;color:black")
                        button(id:"profile" + "${cT}" + "s" + findall.id,orient:"vertical",style:"font-size:13px;",label:"Profile",class:"btn btn-success")
                        button(id:"delete" + "${cT}" + "s" + findall.id,orient:"vertical",style:"font-size:13px;",label:"Remove",class:"btn btn-danger")
                        button(id:"edit" + "${cT}" + "s" + findall.id,orient:"vertical",style:"font-size:13px;",label:"Edit",class:"btn btn-warning")
                        label(value:"${StrStatus}",style:"font-size:13px;color:Green")
                    }

                }

                $('#tablelisttaxi > rows > row > button[label="Profile"]').on('click',{
                    String tranPro = String.valueOf(it.target.id)
                    String SubtranPro = tranPro.substring(tranPro.lastIndexOf("s")+1)
                    session.profileT = SubtranPro
                    session.controlpage = "pt"
                    redirect(uri: "/mtaxi-meter.zul")
                    })
                $('#tablelisttaxi > rows > row > button[label="Remove"]').on('click',{
                    String tranRev = String.valueOf(it.target.id)
                    String SubtranRev = tranRev.substring(tranRev.lastIndexOf("s")+1)
                    def RemoveTaxi = ListDetailsTaxi.findById(SubtranRev)
                    RemoveTaxi.delete()
                    redirect(uri: "/mtaxi-meter.zul")
                    })
                $('#tablelisttaxi > rows > row > button[label="Edit"]').on('click',{
                    String tranEd = String.valueOf(it.target.id)
                    String SubtranEd = tranEd.substring(tranEd.lastIndexOf("s")+1)
                    session.profileT = SubtranEd
                    session.controlpage = "et"
                    redirect(uri: "/mtaxi-meter.zul")
                    })
            }
            }
            

        }
        
    }

      def SearchNoCar(){
        try{
        for(ListDetailsTaxi findall : ListDetailsTaxi.findAll()){
            String strCompare = "projectgrails.TaxiStock : " + session.user
            String strTaxistock = String.valueOf(findall.taxistock)
            if(strCompare.equals(strTaxistock)){
                if(strNocarSearch.equals(findall.Registration)){
                if(findall.Status == 1){
                    StrStatus = "ว่าง"
                }
                else if(findall.Status == 2){
                    StrStatus = "ไม่ว่าง"
                }
                else if(findall.Status == 3){
                    StrStatus = "คืนรถวันนี้"
                }
                count = count + 1
                cT = cT + 1
                $('#tablelisttaxi > rows').append{
                    row(style:"font-size:13px;color:black;background:#B0C4DE;"){
                        label(value:"${count}",style:"font-size:13px;color:black")
                        label(value:"ทะเบียน " + findall.Registration + "   ยี่ห้อ " + findall.Brand + "   สี" + findall.Color,
                        style:"font-size:13px;color:black")
                        button(id:"profile" + "${cT}" + "s" + findall.id,orient:"vertical",style:"font-size:13px;",label:"Profile",class:"btn btn-success")
                        button(id:"delete" + "${cT}" + "s" + findall.id,orient:"vertical",style:"font-size:13px;",label:"Remove",class:"btn btn-danger")
                        button(id:"edit" + "${cT}" + "s" + findall.id,orient:"vertical",style:"font-size:13px;",label:"Edit",class:"btn btn-warning")
                        label(value:"${StrStatus}",style:"font-size:13px;color:Green")
                    }

                }

                $('#tablelisttaxi > rows > row > button[label="Profile"]').on('click',{
                    String tranPro = String.valueOf(it.target.id)
                    String SubtranPro = tranPro.substring(tranPro.lastIndexOf("s")+1)
                    session.profileT = SubtranPro
                    session.controlpage = "pt"
                    redirect(uri: "/mtaxi-meter.zul")
                    })
                $('#tablelisttaxi > rows > row > button[label="Remove"]').on('click',{
                    String tranRev = String.valueOf(it.target.id)
                    String SubtranRev = tranRev.substring(tranRev.lastIndexOf("s")+1)
                    def RemoveTaxi = ListDetailsTaxi.findById(SubtranRev)
                    RemoveTaxi.delete()
                    redirect(uri: "/mtaxi-meter.zul")
                    })
                $('#tablelisttaxi > rows > row > button[label="Edit"]').on('click',{
                    String tranEd = String.valueOf(it.target.id)
                    String SubtranEd = tranEd.substring(tranEd.lastIndexOf("s")+1)
                    session.profileT = SubtranEd
                    session.controlpage = "et"
                    redirect(uri: "/mtaxi-meter.zul")
                    })
            }
            }
            

        }}
        catch(e){
             $('#tablelisttaxi > rows').append{
                    row(style:"font-size:13px;color:black;background:#B0C4DE;"){
                        label(value:"No Data",style:"font-size:13px;color:black")
                    }

                }

        }


    }
}
