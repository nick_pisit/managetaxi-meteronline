package projectgrails


class RegisterComposer extends zk.grails.Composer {

    def afterCompose = { window ->
    	$('#alert1').setVisible(false)

        $('#btn_register').on('click',{
        	String stockname = $('#stockNameRegis').text()
        	String username  = $('#userRegis').text()
        	String password  = $('#passRegis').text()
        	String repassword = $('#RepassRegis').text()
        	String email = $('#emailRegis').text()
        	String phone = $('#phoneRegis').text()
        	String address = $('#addressRegis').text()

        	if(password != repassword){
        		alert("รหัสผ่านไม่ถูกต้อง")
        	}
	        	else if(stockname.equals("") || username.equals("") || 
	        		password.equals("") || repassword.equals("") || email.equals("") ||
	        		phone.equals("") || address.equals("")){
	        		alert("กรุณากรอกข้อมูลให้ครบ")
	        	}
		        	else{
		        		def owner = new Owner()
		        		owner.StockName = stockname
		        		owner.Username = username
		        		owner.Password = password
		        		owner.Email = email
		        		owner.Phone = phone
		        		owner.AddressOwner = address
                        def setting = new Setting()
                        owner.setting = setting
                        def taxistock = new TaxiStock()
                        owner.taxistock = taxistock
                        def membergroup = new Membergroup()
                        owner.membergroup = membergroup
		        		owner.save()
                        def alertdb = new Alertdb()
                        alertdb.statusshow = 0
                        alertdb.save()
		        		$('#stockNameRegis').setText("")
        				$('#userRegis').setText("")
        				$('#passRegis').setText("")
        				$('#RepassRegis').setText("")
        				$('#emailRegis').setText("")
        				$('#phoneRegis').setText("")
        				$('#addressRegis').setText("")
        				
		        		$('#alert1').setVisible(true)
		        		
		        	}
        	
        	})


    	}
}
