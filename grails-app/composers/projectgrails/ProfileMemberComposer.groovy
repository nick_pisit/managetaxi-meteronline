package projectgrails
import org.zkoss.io.*
import org.zkoss.util.media.Media
import org.zkoss.zhtml.Fileupload
import org.zkoss.zk.ui.event.EventListener
import org.zkoss.zk.ui.event.UploadEvent
import org.zkoss.image.Image
import org.zkoss.image.AImage

class ProfileMemberComposer extends zk.grails.Composer {
Media media
Image img
    def afterCompose = { window ->
        update()
        $('#gedit').setVisible(false)
        $('#btn-save').setVisible(false)
        $('#romoveS').setVisible(false)
        $('#btn-edit').on('click',{
        	 $('#memberprofile').setVisible(false)
        	 $('#gedit').setVisible(true)
        	 $('#romoveS').setVisible(false)
        	 $('#btn-save').setVisible(true)
        	 $('#btn-edit').setVisible(false)
        	 update()
        	})
       $('#btn-save').on('click',{
       		$('#memberprofile').setVisible(true)
        	$('#gedit').setVisible(false)
        	$('#romoveS').setVisible(false)
        	$('#btn-save').setVisible(false)
        	$('#btn-edit').setVisible(true)
        	save()
        	update()
       	})
       $('#btn-del').on('click',{
       		def member3 = Members.findById(session.members)
       		member3.delete()
       		$('#romoveS').setVisible(true)
       		$('#btn-save').setVisible(false)
        	$('#btn-edit').setVisible(false)
        	$('#btn-del').setVisible(false)
        	$('#memberprofile').setVisible(false)
        	$('#gedit').setVisible(false)
            $('#upload').setVisible(false)
       	})
       $('#upload').on('upload'){ ev->
            media = ev.getMedia()
            img = (Image) media
            $('#imgs2').setContent(img)
        }
    }
    def update(){
    	def member = Members.findById(session.members)
        byte[] bytes = member.imagedb
        $('#fname').setValue(member.fnameMember)
        $('#lname').setValue(member.lnameMember)
        String Strage = String.valueOf(member.age)
        $('#age').setValue(Strage)
        $('#iden').setValue(member.idennumber)
        $('#tel').setValue(member.tel)
        $('#address').setValue(member.address)
        $('#imgs').setContent(new AImage("nick",bytes))
        $('#imgs2').setContent(new AImage("nick2",bytes))
        $('#txtfname').setText(member.fnameMember)
        $('#txtlname').setText(member.lnameMember)
        $('#comboage').setText(Strage)       
        $('#txtidenid').setText(member.idennumber)
        $('#txttel').setText(member.tel)
        $('#txtaddress').setText(member.address)

    }

    def save(){
    	def member2 = Members.findById(session.members)
    	member2.fnameMember = $('#txtfname').text()
    	member2.lnameMember = $('#txtlname').text()
    	String Strage2 = String.valueOf($('#comboage').getSelectedItem().getLabel())
    	int ageInt = Integer.valueOf(Strage2)
    	member2.age = ageInt
    	member2.tel = $('#txttel').text()
    	member2.idennumber = $('#txtidenid').text()
    	member2.address = $('#txtaddress').text()
        member2.imagedb = $('#imgs2')[0].getContent().getByteData()
    	member2.save()

    }
}
