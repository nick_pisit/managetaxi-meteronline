package projectgrails
import org.zkoss.io.*
import org.zkoss.util.media.Media
import org.zkoss.zhtml.Fileupload
import org.zkoss.zk.ui.event.EventListener
import org.zkoss.zk.ui.event.UploadEvent
import org.zkoss.image.Image
import org.zkoss.image.AImage

class ListMembersComposer extends zk.grails.Composer {
int count = 0
 Media media
 Image img
    def afterCompose = { window ->
    	$('#agemem').setSelectedIndex(0)
        $('#addmember').setVisible(true)
        $('#hideformaddmember').setVisible(false)
        $('#addform').setVisible(false)
        $('#btnAddmember').setVisible(false)
        $('#addmember').on('click',{
        	$('#addmember').setVisible(false)
        	$('#hideformaddmember').setVisible(true)
        	$('#addform').setVisible(true)
        	$('#btnAddmember').setVisible(true)
        	$('#agemem').setSelectedIndex(0)
        	})
        $('#hideformaddmember').on('click',{
        	$('#addmember').setVisible(true)
        	$('#hideformaddmember').setVisible(false)
        	$('#addform').setVisible(false)
        	$('#btnAddmember').setVisible(false)
        	})
        $('#btnAddmember').on('click',{
        	$('#tablemembers > rows > row').detach()
        	addmember()
        	count = 0 
        	update()
        	})
        $('#btnsearchcar').on('click',{
            count = 0 
            searchmem()
            })
        $('#upload').on('upload'){ ev->
            media = ev.getMedia()
            img = (Image) media
            $('#imgs').setContent(img)
        }
        update()
    }
    def addmember(){
    	String fname1 = $('#fname').text()
    	String lname1 = $('#lname').text()
    	String iden1  = String.valueOf($('#idenid').text())
    	String Strage = String.valueOf($('#agemem').getSelectedItem().getLabel())
    	int age1 = Integer.valueOf(Strage)
    	String tel1 = String.valueOf($('#telmember').getValue())
    	String address1 = $('#addressmem').text()
    	if(fname1.equals("") || lname1.equals("") || iden1.equals("")  || tel1.equals("null") || address1.equals("")){
    		alert("ข้อมูลไม่ครบ")
    	}
    	else{
    	def members = new Members()
    	members.fnameMember = fname1
    	members.lnameMember = lname1
    	members.age = age1
    	members.tel = tel1
    	members.idennumber = iden1
    	members.address = address1
        members.imagedb = $('#imgs')[0].getContent().getByteData()

    	def membergroup = Membergroup.findById(session.user)
    	members.membergroup = membergroup
    	members.save()
    	$('#fname').setText("")
    	$('#lname').setText("")
    	$('#idenid').setText("")
    	$('#agemem').setSelectedIndex(0)
    	$('#telmember').setText("")
    	$('#addressmem').setText("")
    }
    }
    def update(){
    	for(Members findmember : Members.findAll()){
    		String stridGroup = String.valueOf(findmember.membergroupId)
    		if(stridGroup.equals(session.user)){
    		String strAge = String.valueOf(findmember.age)
    		count+=1
    		 $('#tablemembers > rows').append{
                    row(style:"font-size:13px;color:black;background:#B0C4DE;"){
                        label(value:"${count}",style:"font-size:13px;color:black")
                        label(value:"ชื่อ " + findmember.fnameMember + "   สกุล " + findmember.lnameMember + "   อายุ " + strAge + " ปี",
                        style:"font-size:13px;color:black")
                        label(value:findmember.idennumber,style:"font-size:13px;color:black")
                        label(value:findmember.address,style:"font-size:13px;color:black")
                        label(value:"0" + findmember.tel,style:"font-size:13px;color:black")
                        button(id:findmember.id,orient:"vertical",style:"font-size:13px;",label:"Profile",class:"btn btn-info")
                    }

                }
                $('#tablemembers > rows > row > button[label="Profile"]').on('click',{
                    String tranmem = String.valueOf(it.target.id)
                    session.members = tranmem
                    session.controlpage = "member"
                    redirect(uri:"/mtaxi-meter.zul")
                    })
            }
    	}
    }
    def searchmem(){
        String Strsearch = $('#searchcar').text()
            if(Strsearch.equals("")){
                alert("ไม่พบข้อมูล")
            }
                else{
                    $('#tablemembers > rows > row').detach()
                    String Strcompare = "projectgrails.Membergroup : " + session.user
                    for(Members membersearch : Members.findAll()){
                        String Strmembergroup = String.valueOf(membersearch.membergroup)
                        if(Strcompare.equals(Strmembergroup) && Strsearch.equals(membersearch.idennumber)){
                                String strAge2 = String.valueOf(membersearch.age)
                                count+=1

                                 $('#tablemembers > rows').append{
                                        row(style:"font-size:13px;color:black;background:#B0C4DE;"){
                                            label(value:"${count}",style:"font-size:13px;color:black")
                                            label(value:"ชื่อ " + membersearch.fnameMember + "   สกุล " + membersearch.lnameMember + "   อายุ " + strAge2 + " ปี",
                                            style:"font-size:13px;color:black")
                                            label(value:membersearch.idennumber,style:"font-size:13px;color:black")
                                            label(value:membersearch.address,style:"font-size:13px;color:black")
                                            label(value:membersearch.tel,style:"font-size:13px;color:black")
                                            button(id:membersearch.id,orient:"vertical",style:"font-size:13px;",label:"Profile",class:"btn btn-info")
                    }

                }
                        }
                         
                    }

                }
                 
    }

    
}
