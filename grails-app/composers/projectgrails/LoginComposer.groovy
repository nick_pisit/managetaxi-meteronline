package projectgrails


class LoginComposer extends zk.grails.Composer {

    def afterCompose = { window ->
    	$('#alert1').setVisible(false)
    	$('#btn_submit').on('click',{
    		String username = $('#user').text()
	        String password = $('#pass').text()
	        if(username.equals("") || password.equals("")){
	        	$('#user').setText("")
	        	$('#pass').setText("")
	        	$('#alert1').setVisible(true)
	        }
	        	else{
	        		for(Owner log : Owner.findAll()){
	        			if(log.Username == username && log.Password == password){
	        				String idOwner = log.id
	        				session.user = idOwner
	        				redirect(uri : "/Mtaxi-meter.zul")

	        			}
	        		}
	        		$('#user').setText("")
	        		$('#pass').setText("")
			        	
	        	}       	 	 
	        	
    		})
        
    }
}
