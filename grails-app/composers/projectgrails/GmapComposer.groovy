package projectgrails
import org.zkoss.gmaps.Ginfo
import org.zkoss.gmaps.Gmaps
import org.zkoss.gmaps.Gmarker
import org.zkoss.zk.ui.Component
import org.zkoss.zk.ui.select.SelectorComposer
import org.zkoss.zk.ui.select.annotation.Listen
import org.zkoss.zk.ui.select.annotation.Wire
import org.zkoss.zul.Doublebox
import org.zkoss.zul.Intbox
import org.zkoss.gmaps.event.MapMouseEvent



class GmapComposer extends zk.grails.Composer {

    def afterCompose = { window ->
    	def tabean = ListDetailsTaxi.findById(session.profileT)
    	$('#info').setContent(tabean.Registration)
    	$('#latitood').setValue($('#gmaps').getLat())
    	$('#longtood').setValue($('#gmaps').getLng())
    	
        $('#inputfocus').on('click',{
        	$('#gmaps').setLat($('#latitood').getValue())
        	$('#gmaps').setLng($('#longtood').getValue())
        	$('#marker').setLat($('#latitood').getValue())
        	$('#marker').setLng($('#longtood').getValue())
        	$('#info').setLat($('#latitood').getValue())
        	$('#info').setLng($('#longtood').getValue())
        	})

        $('#gmaps').on('MapMove',{
        	$('#latitood').setValue($('#gmaps').getLat())
    		$('#longtood').setValue($('#gmaps').getLng())
        	})
    }
}
