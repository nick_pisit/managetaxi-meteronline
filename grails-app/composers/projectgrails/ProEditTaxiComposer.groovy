package projectgrails
import org.zkoss.io.*
import org.zkoss.util.media.Media
import org.zkoss.zhtml.Fileupload
import org.zkoss.zk.ui.event.EventListener
import org.zkoss.zk.ui.event.UploadEvent
import org.zkoss.image.Image
import org.zkoss.image.AImage
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date
import java.io.FileOutputStream
import com.itextpdf.text.Document
import com.itextpdf.text.DocumentException
import com.itextpdf.text.Font
import com.itextpdf.text.Paragraph
import com.itextpdf.text.pdf.BaseFont
import com.itextpdf.text.pdf.PdfWriter
import com.itextpdf.text.pdf.draw.LineSeparator
import com.itextpdf.text.Rectangle
import com.itextpdf.text.Element
import com.itextpdf.text.pdf.draw.LineSeparator
import com.itextpdf.text.Chunk
import com.itextpdf.text.pdf.draw.VerticalPositionMark
import com.itextpdf.text.pdf.ColumnText
import org.zkoss.zk.ui.*
import com.itextpdf.text.Image


class ProEditTaxiComposer extends zk.grails.Composer {
    String StrnameM = ""
    Media media
    Image img
    def afterCompose = { window ->
        checkshow()
        pullmember()
   
        	$('#btn-edit').on('click',{
        		$('#romoveS').setVisible(false)
        		$('#gprofile').setVisible(false)
        		$('#gedit').setVisible(true)
        		$('#btn-save').setVisible(true)
        		$('#btn-edit').setVisible(false)
                $('#btn-jong').setVisible(false)
                $('#txtMember').setDisabled(true)
        		pullEdit()
        		})
        	$('#btn-save').on('click',{
                saveProfile()
                pullAfterSave()
        		$('#romoveS').setVisible(false)
        		$('#gedit').setVisible(false)
	        	$('#gprofile').setVisible(true)
	        	$('#btn-save').setVisible(false)
	        	$('#btn-edit').setVisible(true)
                checkshow()
                $('#romoveS').setVisible(false)
                $('#gedit').setVisible(false)
                $('#gprofile').setVisible(true)
                $('#btn-save').setVisible(false)
                $('#btn-edit').setVisible(true)
        		})
        	$('#btn-del').on('click',{
        		def profile5 = ListDetailsTaxi.findById(session.profileT)
        		profile5.delete()
        		$('#tabean').setValue("")
        		$('#band').setValue("")
        		$('#imagePro').setSrc("images/null.png")
        		$('#statusT').setValue("")
        		$('#memberT').setValue("")
        		$('#gprofile').setVisible(true)
        		$('#gedit').setVisible(false)
        		$('#btn-save').setVisible(false)
        		$('#btn-edit').setVisible(false)
        		$('#btn-del').setVisible(false)
        		$('#btn-track').setVisible(false)
        		$('#romoveS').setVisible(true)
                $('#btn-jong').setVisible(false)
        		})
        	$('#btn-track').on('click',{
        		$('#romoveS').setVisible(false)
        		session.controlpage = "map"
                redirect(uri: "/mtaxi-meter.zul")
        		})
            $('#hideformaddcar').on('click',{
                $('#btn-jong').setVisible(true)
                $('#hideformaddcar').setVisible(false)
                $('#adduse').setVisible(false)
                $('#btn-edit').setDisabled(false)
                
                })
            $('#btn-jong').on('click',{
                $('#btn-jong').setVisible(false)
                $('#hideformaddcar').setVisible(true)
                $('#adduse').setVisible(true)
                $('#btn-edit').setDisabled(false)
                
                    })
                $('#upload').on('upload'){ ev->
                media = ev.getMedia()
                img = (Image) media
                $('#imgs2').setContent(img)
            }
            $('#btn-pdf').on('click',{
                pdftext()
                })
            $('#updatemember').on('click',{
                Date dateout1
                Date datein1 = new Date()
                String strmemlist = String.valueOf($('#members').getSelectedItem().getLabel())
                dateout1 = $('#db5').getValue()
                def mem1  = Members.findByIdennumber(strmemlist)
                def story1 = new StoryMember()
                String strmemname = mem1.id
                def taxi1 = ListDetailsTaxi.findById(session.profileT)
                taxi1.idtaxi = strmemname
                taxi1.save()
                def profile5 = ListDetailsTaxi.findById(session.profileT)
                profile5.Status = 2
                profile5.save()
                def taxi2 = ListDetailsTaxi.findById(session.profileT)
                story1.dateout = dateout1
                story1.idmem = taxi2.idtaxi
                story1.datein = datein1
                story1.save()
                pullAfterSave()
                $('#btn-jong').setVisible(true)
                $('#hideformaddcar').setVisible(false)
                $('#adduse').setVisible(false)
                $('#btn-edit').setDisabled(false)

                })
           
        }
    
    def pullEdit(){
    	def profile2 = ListDetailsTaxi.findById(session.profileT)
        byte[] bytes2 = profile2.imageTaxi
        			$('#txtTabean').setText(profile2.Registration)
        			$('#txtBand').setText(profile2.Brand)
        			String StrColorT2 = String.valueOf(profile2.Color)
        			if(StrColorT2.equals("เขียว-เหลือง")){
        				$('#colorTaxi').setSelectedIndex(0)
        			}
        				else if(StrColorT2.equals("ฟ้า-แดง")){
        					$('#colorTaxi').setSelectedIndex(1)
        				}
        				else if(StrColorT2.equals("ชมพู")){
        					$('#colorTaxi').setSelectedIndex(2)
        				}
        				else if(StrColorT2.equals("ฟ้า")){
        					$('#colorTaxi').setSelectedIndex(3)
        				}
        			String StrStatus2 = String.valueOf(profile2.Status)
        			if(StrStatus2.equals("1")){
        				$('#combostatus').setSelectedIndex(0)
        			}
        				else if(StrStatus2.equals("2")){
        					$('#combostatus').setSelectedIndex(1)
        				}
        				else if(StrStatus2.equals("3")){
        					$('#combostatus').setSelectedIndex(2)
        				}
                        $('#txtMember').setText(StrnameM)

                      $('#imgs2').setContent(new AImage("nick",bytes2))  
    }
    def saveProfile(){
    	def profile3 = ListDetailsTaxi.findById(session.profileT)
    	profile3.Registration = $('#txtTabean').text()
    	profile3.Brand = $('#txtBand').text()
    	String StrColorT3 = String.valueOf($('#colorTaxi').getSelectedItem().getLabel())
    	profile3.Color = StrColorT3
    	String StrStatus3 = String.valueOf($('#combostatus').getSelectedItem().getLabel())
    	if(StrStatus3.equals("ว่าง")){
    		profile3.Status = 1
            profile3.idtaxi = null

    	}
    		else if(StrStatus3.equals("ไม่ว่าง")){
    			profile3.Status = 2
                $('#btn-jong').setVisible(false)
    		}
    		else if(StrStatus3.equals("ส่งคืนวันนี้")){
    			profile3.Status = 3
                $('#btn-jong').setVisible(false)
    		}
        profile3.imageTaxi = $('#imgs2')[0].getContent().getByteData()
    	profile3.save()
         
    }
    def pullAfterSave(){
    	def profile4 = ListDetailsTaxi.findById(session.profileT)
        byte[] bytes3 = profile4.imageTaxi
    	$('#tabean').setValue(profile4.Registration)
        	$('#band').setValue(profile4.Brand)
        	String StrcolorT4 = String.valueOf(profile4.Color)
        		if(StrcolorT4.equals("เขียว-เหลือง")){
        			$('#imagePro').setSrc("images/gy.png")
        		}	
        			else if(StrcolorT4.equals("ฟ้า-แดง")){
        				$('#imagePro').setSrc("images/br.png")
        			}
        			else if(StrcolorT4.equals("ชมพู")){
        				$('#imagePro').setSrc("images/p.png")
        			}
        			else if(StrcolorT4.equals("ฟ้า")){
        				$('#imagePro').setSrc("images/b.png")
        			}
        	String StrStatus4 = String.valueOf(profile4.Status)
        	if(StrStatus4.equals("1")){
        		$('#statusT').setValue("ว่าง")
                $('#btn-jong').setVisible(true)
        	}
        		else if(StrStatus4.equals("2")){
        			$('#statusT').setValue("ไม่ว่าง")
                    $('#hideformaddcar').setVisible(false)
                    $('#btn-jong').setVisible(false)
        		}
        		else if(StrStatus4.equals("3")){
        			$('#statusT').setValue("คืนรถวันนี้")
                    $('#hideformaddcar').setVisible(false)
                    $('#btn-jong').setVisible(false)
        		}
                $('#imgs1').setContent(new AImage("nick",bytes3))

            if(profile4.idtaxi == null){
                $('#memberT').setValue("ไม่มีผู้เช่า")
                $('#btn-pdf').setVisible(false)
            }
            else{
                def memname2 = Members.findById(profile4.idtaxi) 
                $('#memberT').setValue(memname2.fnameMember)
                $('#hideformaddcar').setVisible(false)
                $('#btn-jong').setVisible(false)
                $('#btn-pdf').setVisible(false)
                $('#btn-pdf').setVisible(false)
                $('#btn-pdf').setVisible(true)
            }

    }
    def checkshow(){
        if((session.controlpage).equals("pt")){
            $('#hideformaddcar').setVisible(false)
            $('#gedit').setVisible(false)
            $('#gprofile').setVisible(true)
            $('#romoveS').setVisible(false)
            $('#btn-save').setVisible(false)
            def profile = ListDetailsTaxi.findById(session.profileT)
            $('#tabean').setValue(profile.Registration)
            $('#band').setValue(profile.Brand)
            $('#adduse').setVisible(false)
            byte[] bytes = profile.imageTaxi
            $('#imgs1').setContent(new AImage("nick",bytes))
            String StrcolorT = String.valueOf(profile.Color)
                if(StrcolorT.equals("เขียว-เหลือง")){
                    $('#imagePro').setSrc("images/gy.png")
                }   
                    else if(StrcolorT.equals("ฟ้า-แดง")){
                        $('#imagePro').setSrc("images/br.png")
                    }
                    else if(StrcolorT.equals("ชมพู")){
                        $('#imagePro').setSrc("images/p.png")
                    }
                    else if(StrcolorT.equals("ฟ้า")){
                        $('#imagePro').setSrc("images/b.png")
                    }
            String StrStatus = String.valueOf(profile.Status)
            if(StrStatus.equals("1")){
                $('#statusT').setValue("ว่าง")
                $('#btn-jong').setVisible(true)
                $('#memberT').setValue(StrnameM)
            }
                else if(StrStatus.equals("2")){
                    $('#statusT').setValue("ไม่ว่าง")
                    $('#hideformaddcar').setVisible(false)
                    $('#btn-jong').setVisible(false)
                    $('#memberT').setValue(StrnameM)
                }
                else if(StrStatus.equals("3")){
                    $('#statusT').setValue("คืนรถวันนี้")
                    $('#hideformaddcar').setVisible(false)
                    $('#btn-jong').setVisible(false)
                    $('#memberT').setValue(StrnameM)
                }


            if(profile.idtaxi == null){
                $('#memberT').setValue("ไม่มีผู้เช่า")
                $('#btn-pdf').setVisible(false)
            }
            else{
                def memname3 = Members.findById(profile.idtaxi) 
                $('#memberT').setValue(memname3.fnameMember)
                $('#hideformaddcar').setVisible(false)
                $('#btn-jong').setVisible(false)
                $('#btn-pdf').setVisible(false)
                $('#btn-pdf').setVisible(true)
            }

        }
            else if((session.controlpage).equals("et")){
                $('#gprofile').setVisible(false)
                $('#romoveS').setVisible(false)
                $('#gedit').setVisible(true)
                $('#btn-save').setVisible(true)
                $('#btn-edit').setVisible(false)
                $('#btn-jong').setVisible(false)
                $('#hideformaddcar').setVisible(false)
                $('#adduse').setVisible(false)
                $('#txtMember').setDisabled(true)
                pullEdit()
            }
            
    }


    public void pdftext(){
        def taxi = ListDetailsTaxi.findById(session.profileT)
        def member = Members.findById(taxi.idtaxi)
        Rectangle pagesize = new Rectangle(540f, 597f)
        Document document = new Document(pagesize,  1.5f, 1.5f, 1.5f, 1.5f)
        LineSeparator line = new LineSeparator()

        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(application.getRealPath("/ext/pdf/${session.user}.pdf")))

        document.open()

        BaseFont basef
        Font font,font2


        basef = BaseFont.createFont(application.getRealPath("ARIALUNI.ttf"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED)
        font = new Font(basef, 12)
        font2 = new Font(basef, 18)
        String storecarname
        String addresscompa
        String agestr = String.valueOf(member.age)
        byte[] bytes = member.imagedb
        def storename1 = Owner.findById(session.user)
        storecarname = String.valueOf(storename1.StockName)
        Paragraph storecarnameP = new Paragraph("อู่ " + storecarname,font2)
        storecarnameP.setAlignment(Element.ALIGN_CENTER)
        Paragraph companyaddress = new Paragraph(addresscompa,font)
        companyaddress.setAlignment(Element.ALIGN_CENTER)
        Image img = Image.getInstance(bytes)
        img.setAlignment(Element.ALIGN_CENTER)
        img.scaleToFit(200,250)
        document.add(storecarnameP)
        document.add(companyaddress)
        document.add(new Paragraph(" "))
        document.add(new Paragraph(" "))
        Paragraph head1 = new Paragraph("      ข้อมูลแท็กซี่",font)
        head1.setAlignment(Element.ALIGN_LEFT)
Paragraph right2 = new Paragraph("      ทะเบียน : ${taxi.Registration}   " + "ยี่ห้อ : ${taxi.Brand}   " + "สี : ${taxi.Color}",font)
        right2.setAlignment(Element.ALIGN_LEFT)
        Paragraph enter = new Paragraph("  ",font)
        enter.setAlignment(Element.ALIGN_CENTER)
        document.add(enter)
        Paragraph head2 = new Paragraph("      ข้อมูลผู้เช่า",font)
        head2.setAlignment(Element.ALIGN_LEFT)
Paragraph right4 = new Paragraph("      ชื่อ : ${member.fnameMember}" + "   นามสกุล : ${member.lnameMember}" + "   อายุ : ${agestr}" + "   Tel : ${member.tel}",font)
        right4.setAlignment(Element.ALIGN_LEFT)
        Paragraph right5 = new Paragraph("      เลขประจำตัวประชาชน : ${member.idennumber}" + "   ที่อยู่ : ${member.address}",font)
        right5.setAlignment(Element.ALIGN_LEFT)
        document.add(head1)
        document.add(right2)
        document.add(head2)
        document.add(img)
        document.add(right4)
        document.add(right5)
        def storymember2 = StoryMember.findByIdmem(taxi.idtaxi)
        String strdatein = String.valueOf(storymember2.datein)
        String substrdatein = strdatein.substring(0,16)
        String strdateout = String.valueOf(storymember2.dateout)
        String substrdateout = strdateout.substring(0,16)
        Paragraph datein = new Paragraph("      วัน/เวลาที่เช่า : ${substrdatein}" + "   วัน/เวลาที่คืน : ${substrdateout}",font)
        datein.setAlignment(Element.ALIGN_LEFT)
        document.add(datein)
        Paragraph a = new Paragraph("  ",font)
        a.setAlignment(Element.ALIGN_CENTER)
        document.add(a)
        Paragraph b = new Paragraph("  ",font)
        b.setAlignment(Element.ALIGN_CENTER)
        document.add(b)
        Paragraph c = new Paragraph("  ",font)
        c.setAlignment(Element.ALIGN_CENTER)
        document.add(c)
        Paragraph aa = new Paragraph("  ",font)
        aa.setAlignment(Element.ALIGN_CENTER)
        document.add(aa)
        Paragraph bb = new Paragraph("  ",font)
        bb.setAlignment(Element.ALIGN_CENTER)
        document.add(bb)
        Paragraph cc = new Paragraph("  ",font)
        cc.setAlignment(Element.ALIGN_CENTER)
        document.add(cc)
        Paragraph name = new Paragraph("ลายเซ็นต์ผู้เช่า",font)
        name.setAlignment(Element.ALIGN_CENTER)
        document.add(name)
        Paragraph aaaa = new Paragraph("  ",font)
        aaaa.setAlignment(Element.ALIGN_CENTER)
        document.add(aaaa)
        Paragraph name2 = new Paragraph("....................................",font)
        name2.setAlignment(Element.ALIGN_CENTER)
        document.add(name2)
        Paragraph bbbb = new Paragraph("  ",font)
        bbbb.setAlignment(Element.ALIGN_CENTER)
        document.add(bbbb)
        Paragraph name3 = new Paragraph("${member.fnameMember}" + "${member.lnameMember}",font)
        name3.setAlignment(Element.ALIGN_CENTER)
        document.add(name3)
        document.close()
        session.controlpage = "pdf"
        redirect(uri: "/mtaxi-meter.zul")
    }

def pullmember(){
    for(Members listmem : Members.findAll()){
                    String strgroup = String.valueOf(listmem.membergroup)
                    String cpStrgroup = "projectgrails.Membergroup : " + session.user
                    if(cpStrgroup.equals(strgroup)){
                        String stridnumber = String.valueOf(listmem.idennumber)
                        $('#members').appendItem(stridnumber)  
                    }
                }
}
}
