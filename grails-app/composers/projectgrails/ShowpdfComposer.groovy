package projectgrails


class ShowpdfComposer extends zk.grails.Composer {

    def afterCompose = { window ->
        $('#iframe').setSrc("ext/pdf/${session.user}.pdf")
    }
}
