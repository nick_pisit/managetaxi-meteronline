package projectgrails

class TaxiStock {
	static belongsTo = [owner : Owner]
	static hasMany = [listdetailstaxi : ListDetailsTaxi]
    static mapping = {
      listdetailstaxi cascade:"all-delete-orphan"
    }
    static constraints = {

    }
}
