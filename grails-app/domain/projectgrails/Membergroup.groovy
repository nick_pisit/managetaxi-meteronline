package projectgrails

class Membergroup {
	static belongsTo = [owner:Owner]
	static hasMany = [members:Members]
    static constraints = {
    }
}
