package projectgrails

class ListDetailsTaxi {
	String Registration
	String Brand
	String Color
	int Status  //1 = easy , 2 = bysy , 3 = return today
	byte [] imageTaxi = new byte[0]
	String idtaxi
	static belongsTo = [taxistock : TaxiStock]
    static constraints = {
    	imageTaxi maxSize:50*1204*1204
    	idtaxi nullable: true
    }
}
