package projectgrails

class Members {
	String fnameMember
	String lnameMember
	int age
	String tel
	String idennumber
	String address
	byte[] imagedb = new byte[0]
	static belongsTo = [membergroup:Membergroup]
    static constraints = {
    	imagedb maxSize:50*1204*1204
    }
}
