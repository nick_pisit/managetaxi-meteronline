package projectgrails

class Owner {
	Setting setting
	TaxiStock taxistock
	Membergroup membergroup
	String StockName
	String Username
	String Password
	String Email
	String Phone
	String AddressOwner
    static constraints = {
    setting unique: true
    taxistock unique: true
    membergroup unique: true	 
    }
}
